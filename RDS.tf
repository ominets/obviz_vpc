# Create RDS
# terraform aws create RDS
resource "aws_db_instance" "aws_rds" {
  identifier                = "myrds"  
  engine                    = "mysql"
  engine_version            = "5.7"
  instance_class            = "db.t2.micro"
  name                      = "mydb"
  username                  = "obvizdbtest"
  password                  = "secretpass"
  allocated_storage         = 20
  parameter_group_name      = "default.mysql5.7"
  skip_final_snapshot       = true
  port                      = 3306
  availability_zone         = "ap-southeast-2a"
  multi_az                  = false
  publicly_accessible       = false
  vpc_security_group_ids    = ["${aws_security_group.database-security-group.id}"]
  

  tags      = {
    Name    = "Obviz RDS"
  }
}